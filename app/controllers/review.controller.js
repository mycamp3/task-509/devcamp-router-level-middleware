const { response } = require("express");
const reviewModel = require("../models/review.model");
const courseModel = require("../models/course.model");

const mongoose = require("mongoose");

const createReview = async (req, res) => {
    // B1: lấy DL
    const {
        courseid,
        stars,
        note
    } = req.body;

    // B2: Validate

    if (!mongoose.Types.ObjectId.isValid(courseid)) {
        return res.status(400).json({
            "status": "Bad request",
            "message": "Courseid not valid"
        })
    }

    // Star là số nguyên, lớn hơn 0 & nhỏ hơn 6
    // Number.isInteger(stars) && stars > 0 && stars < 6

    if (stars === undefined || !(Number.isInteger(stars) && 6 > stars > 0)) {
        return res.status(400).json({
            "status": "Bad request",
            "message": "Stars not valid"
        })
    }

    //B3: Thao tác với CSDL

    try {
        var newReview = {
            stars: stars,
            note: note
        }
        const response = await reviewModel.create(newReview)
        // thêm id vào mảng review của course
        const result = await courseModel.findByIdAndUpdate(courseid, {
            $push: { review: response._id }
        })
        return res.status(201).json({
            message: "Tạo thành công",
            data: result
        })
    } catch (error) {
        // dùng các hệ thống thu thập lỗi để xl thu thập error
        return res.status(500).json({
            message: "Internal Server Error"
        })
    }


}



const getAllReviews = async (req, res) => {
    // B1:
    const courseid = req.query.courseid;
    if (courseid !== undefined && !mongoose.Types.ObjectId.isValid(courseid)) {
        return res.status(400).json({
            "status": "Bad request",
            "message": "Courseid not valid"
        })
    }

    // B2:
    // B3: 
    try {
        if (courseid === undefined) {
            const result = await reviewModel.find();

            return res.status(200).json({
                message: "Lấy ds reviews thành công",
                data: result
            })
        } else {
            const courseInfo = await courseModel.findById(courseid).populate('review');
            return res.status(200).json({
                message: "Lấy ds reviews của course thành công",
                data: courseInfo.review
            })
        }

    } catch (error) {
        // dùng các hệ thống thu thập lỗi để xl thu thập error
        return res.status(500).json({
            message: "Có lỗi xảy ra"
        })
    }
}

const getReviewById = async (req,res) => {
    // B1: Thu thập dl
    const reviewid = req.params.reviewid;
    // B2: Validate
    if(!mongoose.Types.ObjectId.isValid(reviewid)) {
        return res.status(400).json({
            message: "Reviewid not valid"
        }) 
    }
    // B3: Xl dl
    try {
        const result = await reviewModel.findById(reviewid);

        if(result) {
            return res.status(200).json({
                message: "Lấy thông tin review thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thtin review"
            })
        }

    } catch(error) {
        // dùng các hệ thống thu thập lỗi để xl thu thập error
        return res.status(500).json({
            message: "Có lỗi xảy ra"
        })
    }
}

const updateReviewById = async (req,res) => {
    // B1: Thu thập dl
    const reviewid = req.params.reviewid;
    const {
        stars,
        note
    } = req.body;

    // B2: Validate
    if(!mongoose.Types.ObjectId.isValid(reviewid)) {
        return res.status(400).json({
            message: "Reviewid not valid"
        }) 
    }

    if(stars !== undefined && !(Number.isInteger(stars) && 6 > stars > 0)) {
        return res.status(400).json({
            message: "Stars ko hợp lệ"
        })
    }

    // B3: Xl dl
    try {
        var newUpdateReview = {};
        if (stars) {
            newUpdateReview.stars = stars
        }
        if (note) {
            newUpdateReview.note = note
        }

        const result = await reviewModel.findByIdAndUpdate(reviewid, newUpdateReview);

        if(result) {
            return res.status(200).json({
                message: "Update thông tin review thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thtin review"
            })
        }

    } catch (error) {
        // dùng các hệ thống thu thập lỗi để xl thu thập error
        return res.status(500).json({
            message: "Có lỗi xảy ra"
        })
    }
}

const deleteReviewById = async (req,res) => {
    // B1: Thu thập dl
    const reviewid = req.params.reviewid;
    // B2: Validate
    if(!mongoose.Types.ObjectId.isValid(reviewid)) {
        return res.status(400).json({
            message: "Reviewid ko hợp lệ"
        }) 
    }
    // B3: Xl dl
    try {
        const result = await reviewModel.findByIdAndDelete(reviewid);

        if(result) {
            return res.status(200).json({
                message: "Xóa thông tin review thành công",
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thtin review"
            })
        }

    } catch(error) {
        // dùng các hệ thống thu thập lỗi để xl thu thập error
        return res.status(500).json({
            message: "Có lỗi xảy ra"
        })
    }
}

module.exports = {
    createReview,
    getAllReviews,
    getReviewById,
    updateReviewById,
    deleteReviewById
}
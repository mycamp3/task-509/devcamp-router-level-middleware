const getAllReviewMiddleware = (req, res, next) => {
    console.log("GET all review middleware");
    
    next();
}

const getAllReviewOfCourseMiddleware = (req, res, next) => {
    console.log("GET all review of course middleware");
    
    next();
}

const createReviewMiddleware = (req, res, next) => {
    console.log("POST review middleware");
    
    next();
}
const getReviewByIdMiddleware = (req, res, next) => {
    console.log("GET review by id middleware");
    
    next();
}
const updateReviewMiddleware = (req, res, next) => {
    console.log("UPDATE review middleware");
    
    next();
}
const deleteReviewMiddleware = (req, res, next) => {
    console.log("DELETE review middleware");
    
    next();
}
module.exports = {
    getAllReviewMiddleware,
    getAllReviewOfCourseMiddleware,
    createReviewMiddleware,
    getReviewByIdMiddleware,
    updateReviewMiddleware,
    deleteReviewMiddleware
}
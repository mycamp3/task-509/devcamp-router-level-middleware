const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const courseSchema = new Schema({
    title: {
        type: String,
        requires: true,
        unique: true
    },
    description: {
        type: String,
        required: false
    },
    noStudent: {
        type: Number,
        default: 0
    },
    //Nếu 1 course có 1 review => bỏ [], dùng {}
    review: [
        {
            type: mongoose.Types.ObjectId,
            ref: "Review"
        }
    ]
},{
    timestamps: true
});

module.exports = mongoose.model("Course", courseSchema);
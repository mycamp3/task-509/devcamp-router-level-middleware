const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const reviewSchema = new Schema({
    stars: {
        type: Number,
        default: 0
    },
    note: {
        type: String,
        required: false
    }
}, {
    timestamps: true
});

module.exports = mongoose.model("Review", reviewSchema);
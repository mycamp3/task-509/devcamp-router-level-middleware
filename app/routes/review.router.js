const express = require("express");

const {
    getAllReviewMiddleware,
    createReviewMiddleware,
    getReviewByIdMiddleware,
    deleteReviewMiddleware,
    updateReviewMiddleware
} = require("../middlewares/review.middleware")

const router = express.Router();
const reviewController = require("../controllers/review.controller");

router.use((req, res, next) => {
    console.log("Request URL review: ", req.url);
    next();
})

router.get("/", getAllReviewMiddleware, reviewController.getAllReviews);

router.post("/", createReviewMiddleware, reviewController.createReview);

router.get("/:reviewid", getReviewByIdMiddleware, reviewController.getReviewById);

router.put("/:reviewid", updateReviewMiddleware, reviewController.updateReviewById);

router.delete("/:reviewid", deleteReviewMiddleware, reviewController.deleteReviewById);

module.exports = router;
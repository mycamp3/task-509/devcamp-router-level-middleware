// import thư viện express
// import express from express

const express = require('express');

const mongoose = require('mongoose');
//b2 khởi tạp app express
const app = new express();

//b3 cống khởi tạo api
const port = 8000;

const courseRouter = require("./app/routes/course.router");
const reviewRouter = require("./app/routes/review.router");


// cấu hình sử dụng json
app.use(express.json());

// 506: Middleware

// Middleware console log ra thời gian hiện tại
app.use((req, res, next) => {
    console.log("Thời gian hiện tại: ", new Date());

    next();
})

// Middleware log ra request method
app.use((req, res, next) => {
    console.log("Request method:", req.method);
    next();
})

// khai báo kết nối mongoDB qua mongoose
mongoose.connect("mongodb://127.0.0.1:27017/R46_Course_Review")
    .then(() => {
        console.log("MongoDB connected");
    })
    .catch((err) => {
        console.log(err);
    });
//khai báo các api
app.get('/', (request, response) => {
    let today = new Date();
    let message = `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`;
    //response.send(message);
    response.json({
        message
    })
})

//505.20

app.post('/post-method', (req, res) => {
    console.log('Post - Method');
    res.json({
        message: "Post - Method"
    })
})

app.put('/put-method', (req, res) => {
    console.log('Put - Method');
    res.json({
        message: "Put - Method"
    })
})


app.delete('/delete-method', (req, res) => {
    console.log('Delete - Method');
    res.json({
        message: "Delete - Method"
    })
})

//khai báo get param
app.get('/get-data/:para1/:param2', (req, res) => {
    let param1 = req.params.para1;
    let param2 = req.params.param2;
    res.json({
        param1,
        param2
    })
})

//khai báo get query string
app.get('/query-data/:id', (req, res) => {
    let id = req.params.id;
    let query = req.query;
    let name = query.name
    res.json({
        id,
        name,
        query
    })
})

//khai báo request body 
app.post('/update-data', (req, res) => {
    let body = req.body;
    res.json({
        body
    })
})
// sử dụng router 
app.use("/api/v1/courses", courseRouter);
app.use("/api/v1/reviews", reviewRouter);

//b4: start app
app.listen(port, () => {
    console.log(`app listening on port ${port}`);
})

module.exports = app
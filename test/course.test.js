const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../index');

const should = chai.should();

chai.use(chaiHttp);

describe("TEST COURSE CRUD API", () => {
    describe("Test get all course", () => {
        it("Should return array of all course", (done) => {
            chai.request(server)
                .get('/api/v1/courses')
                .end((err, res) => {
                    console.log(res.body);
                    res.should.have.status(200);
                    res.body.data.should.be.a('array');
                    done();
                })
               
        })
    })
    describe("Test create new course", () => {
        it("should create and return new course", (done) => {
            let newCourse = {
                "reqTitle": "Valid CSS Title",
                "reqDescription": "Front-end CSS",
                "reqStudent": 15
            };
            chai.request(server)
                .post('/api/v1/courses')
                .send(newCourse)
                .end((err, res) => {
                    console.log(res.body);
                    res.should.have.status(201);
                    res.body.data.should.be.a('object');
                    res.body.data.should.have.property('title').eql(newCourse.reqTitle);
                    res.body.data.should.have.property('description').eql(newCourse.reqDescription);
                    done();
                });
        });
    });
})